
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <GL/gl.h>

#include <iostream>

#include "PainterMultipass.h"

using namespace std;

PainterMultipass::PainterMultipass():
    m_perturbTextureCoordinates(false)
{
    // nop
}

void PainterMultipass::paintMesh(const Mesh& mesh, const Vector& eye, const Vector& light) const {
    if (!mesh.hasMaterial()) {
        return;
    }

    const Material& material = mesh.getMaterial();

    if (!material.hasDetail() || !material.hasBumpmap()) {
        cerr << "PainterMultipass::paintMesh(): material does not have detail or bumpmap layer" << endl;
        exit(1);
    }

    glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_LIGHTING_BIT | GL_TEXTURE_BIT);

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material.getAmbientColor().toArray());
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material.getDiffuseColor().toArray());
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material.getSpecularColor().toArray());
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, material.getEmissiveColor().toArray());
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, material.getSpecularShininess());

    // === First Pass ===

    glEnable(GL_TEXTURE_2D);

    // Turn off the light during the bumpmap step
    glDisable(GL_LIGHTING);

    // Draw the cube with the bumpmap texture
    glBindTexture(GL_TEXTURE_2D, material.getBumpmapHandle());

    const vector<const Triangle*>& triangles = mesh.getTriangles();
    for (vector<const Triangle*>::const_iterator iter = triangles.begin();
        iter != triangles.end();
        iter++) {
        const Triangle* triangle = *iter;
        paintTriangle(*triangle, eye, light);
    }

    // === Second Pass ===

    // Draw only where we've drawn before
    glDepthFunc(GL_EQUAL);

    // Draw the cube with the inverted bumpmap texture
    glBindTexture(GL_TEXTURE_2D, material.getInvertedBumpmapHandle());

    // Blenging(GL_ONE, GL_ONE) will add the previous bump value to the current 
    // (inverted) bump value which is equivalent to subtracting the current bump 
    // value from the perturbed bump value
    glBlendFunc(GL_ONE, GL_ONE);
    glEnable(GL_BLEND);

    m_perturbTextureCoordinates = true;
    for (vector<const Triangle*>::const_iterator iter = triangles.begin(); iter != triangles.end(); iter++) {
        const Triangle* triangle = *iter;
        paintTriangle(*triangle, eye, light);
    }
    m_perturbTextureCoordinates = false;

    // === Third Pass ===

    // Draw the cube with the detail texture modulated by the bumpmap
    glBindTexture(GL_TEXTURE_2D, material.getDetailHandle());

    // Blending(GL_DST_COLOR, GL_SRC_COLOR) multiplies by 2:
    //   (Cdst * Csrc) + (Csrc * Cdst) = 2 (Csrc * Cdst)
    // This is why the bumpmap texture is scaled by 50%
    glBlendFunc(GL_DST_COLOR, GL_SRC_COLOR);
    glEnable(GL_BLEND);

    glEnable(GL_LIGHTING);

    for (vector<const Triangle*>::const_iterator iter = triangles.begin(); iter != triangles.end(); iter++) {
        const Triangle* triangle = *iter;
        paintTriangle(*triangle, eye, light);
    }

    glPopAttrib();
}

void PainterMultipass::paintTriangle(const Triangle& triangle, const Vector& eye, const Vector& light) const {
    float strength = 0.01f;

    glBegin(GL_TRIANGLES);

    for (int i = 0; i < 3; i++) {
        const Vector& location = triangle.getVertex(i).getLocation();

        const Vector& normal = triangle.getNormal(i);
        glNormal3fv(normal.toArray());

        const Vector& texture = triangle.getTextureCoordinates(i);
        if (m_perturbTextureCoordinates) {
            // Bring the light vector into tangent space
            // Once the light vector has been brought into texture
            // space, its X and Y coordinates represent the amount
            // of perturbation that should be applied to the s- and 
            // t- coordinates.
            Vector vertexToLight = light.subtract(location).multiplyLeft(
                triangle.getObjectToTangentSpaceMatrix(i)).normalize();

            glTexCoord2f(
                texture[Vector::X] + vertexToLight[Vector::X] * strength, 
                texture[Vector::Y] + vertexToLight[Vector::Y] * strength);
        } else {
            glTexCoord2f(texture[Vector::X], texture[Vector::Y]);
        }

        glVertex3fv(location.toArray());
    }

    glEnd();
}
