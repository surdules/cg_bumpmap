
#ifndef __PAINTERMULTIPASS_H
#define __PAINTERMULTIPASS_H

#include "Vector.h"

#include "Mesh.h"
#include "Triangle.h"

#include "Painter.h"

/**
    @file PainterMultipass.h
    @brief Declares the PainterMultipass class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief Implements the Painter visitor to draw a Model with detail and bumpmapping
    on the screen.
    
    The drawing process uses multiple passes and blending between them (instead of 
    multitexture facilities).
*/
class PainterMultipass: public Painter {
private:
    /** 
        @brief If true, perturb the current texture coordinates towards the light.

        This value is set to true in #paintMesh in order to perturb the current
        layer towards the light in such a way that, when it is blended against
        the layer underneath, it adds or subtracts to produce a normalmap.
    */
    mutable bool m_perturbTextureCoordinates;

public:
    /** @brief Initialize the object with defaults. */
    PainterMultipass();

    /** 
        @brief Draw the Mesh in multiple passes.

        This method does not use the multitexture facilities of the card: it only
        uses regular blending effects:

        <ol>
            <li>The first pass draws the regular bumpmap.</li>
            <li>The second pass draws the inverted bumpmap perturbed on top of it 
            (effectively subtracts it at an offset): this generates the normal map.</li>
            <li>The third pass blends the detail texture with the normal map.</li>
        </ol>
    */
    virtual void paintMesh(const Mesh& mesh, const Vector& eye, const Vector& light) const;

    /**
        @brief Draw a Triangle taking into account the texture coordinate
        perturbation.

        For every vertex, compute the Vector connecting that vertex to 
        the light. Bring that Vector into tangent space using
        Triangle#getObjectToTangentSpaceMatrix. Once in tangent space, 
        the X and Y coordinates represent the amount by which the bumpmap 
        texture should be perturbed before subtracted from itself in order 
        to compute the normalmap. 

        This can be intuitively understood as follows: if the light is 
        perpendicular to our texture (colinear with the face normal) at 
        the current vertex, then the texture should not be perturbed at 
        all: when the light (=normal) is brought into tangent space, it
        corresponds to the Z axis, that is to the vector (0, 0, 1). The
        X and Y coordinates of the light (=normal) do not perturb the
        texture.
    */
    virtual void paintTriangle(const Triangle& triangle, const Vector& eye, const Vector& light) const;
};

#endif // __PAINTERMULTIPASS_H
