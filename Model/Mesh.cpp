
#include "Model.h"

#include "Mesh.h"

using namespace std;

Mesh::Mesh(const Model& model) :
    m_model(model) {
    // nop
}

const char* Mesh::initialize(const char* ptr) {
    ptr += sizeof(char); // flags
    ptr += 32; // name
    
    int numTriangles = *(short*)ptr;
    ptr += sizeof(short);
    
    for (int i = 0; i < numTriangles; i++) {
        const Triangle& triangle = m_model.getTriangle(*(short*)ptr);
        m_triangles.push_back(&triangle);
        ptr += sizeof(short);
    }
    
    m_materialIndex = *(char*)ptr;
    ptr += sizeof(char);
    
    return ptr;
}

const Model& Mesh::getModel() const {
    return m_model;
}

bool Mesh::hasMaterial() const {
    return (m_materialIndex != -1);
}

const Material& Mesh::getMaterial() const {
    return m_model.getMaterial(m_materialIndex);
}

const vector<const Triangle*>& Mesh::getTriangles() const {
    return m_triangles;
}

ostream& operator<< (ostream& out, const Mesh& mesh)
{
    for (vector<const Triangle*>::const_iterator iter = mesh.m_triangles.begin();
        iter != mesh.m_triangles.end();
        iter++) {
        out << "#Triangle START" << endl;
        out << *iter;
        out << "#Triangle END" << endl;
    }

    return out;
}
