
#include <assert.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <GL/gl.h>

#include <iostream>
#include <fstream>

#include <set>

#include "MS3D.h"

#include "Painter.h"

#include "Model.h"

using namespace std;

Model::Model():
    m_matrix(Matrix::IDENTITY),
    m_painter(new Painter())
{
    // nop
}

Model::~Model() {
    for (vector<Mesh*>::iterator mesh = m_meshes.begin();
        mesh != m_meshes.end();
        mesh++) {
        delete *mesh;
    }
    
    for (vector<Triangle*>::iterator triangle = m_triangles.begin();
        triangle != m_triangles.end();
        triangle++) {
        delete *triangle;
    }
    
    for (vector<Vertex*>::iterator vertex = m_vertices.begin();
        vertex != m_vertices.end();
        vertex++) {
        delete *vertex;
    }
    
    for (vector<Material*>::iterator material = m_materials.begin();
        material != m_materials.end();
        material++) {
        delete *material;
    }

    if (m_painter) {
        delete m_painter;
    }
}

void Model::loadModel(const char* fileName) {
    assert(fileName);
    ifstream inputFile(fileName, ios::in | ios::binary);
    if (!inputFile) {
        cerr << "Model::loadModel(): file '" << fileName << "' not found" << endl;
        exit(1);
    }
    
    inputFile.seekg(0, ios::end);
    long fileSize = inputFile.tellg();
    inputFile.seekg(0, ios::beg);
    
    char* buffer = new char[fileSize];
    inputFile.read(buffer, fileSize);
    inputFile.close();
    
    const char* ptr = buffer;
    
    MS3DHeader* header = (MS3DHeader*) ptr;
    ptr += sizeof(MS3DHeader);
    
    assert(strncmp(header->m_ID, "MS3D000000", 10) == 0);
    assert(header->m_version >= 3 && header->m_version <= 4);
    
    int i;
    
    int numVertices = *(short*) ptr;
    assert(numVertices >= 0);
    ptr += sizeof(short);
    
    for (i = 0; i < numVertices; i++) {
        Vertex* vertex = new Vertex((MS3DVertex*) ptr);
        ptr += sizeof(MS3DVertex);
        m_vertices.push_back(vertex);
    }
    
    int numTriangles = *(short*) ptr;
    assert(numTriangles >= 0);
    ptr += sizeof(short);
    
    for (i = 0; i < numTriangles; i++) {
        Triangle* triangle = new Triangle((MS3DTriangle*) ptr, *this);
        ptr += sizeof(MS3DTriangle);
        m_triangles.push_back(triangle);
    }
    
    int numMeshes = *(short*) ptr;
    assert(numMeshes >= 0);
    ptr += sizeof(short);
    
    for (i = 0; i < numMeshes; i++) {
        Mesh* mesh = new Mesh(*this);
        ptr = mesh->initialize(ptr);
        m_meshes.push_back(mesh);
    }
    
    int numMaterials = *(short*) ptr;
    assert(numMaterials >= 0);
    ptr += sizeof(short);
    
    for (i = 0; i < numMaterials; i++) {
        Material* material = new Material((MS3DMaterial*) ptr);
        ptr += sizeof(MS3DMaterial);
        m_materials.push_back(material);
    }
    
    delete [] buffer;
    
    setupNeighborMap();
}

void Model::setupNeighborMap() {
    map<int, set<const Triangle*> > edges;
    
    for (vector<Triangle*>::iterator triangleIterator = m_triangles.begin(); 
        triangleIterator != m_triangles.end();
        triangleIterator++) {
        Triangle* triangle = *triangleIterator;
        
        for (int i = 0; i < 3; i++) {
            int edge = triangle->getEdgeIndex(i);
            assert(edges[edge].find(triangle) == edges[edge].end());
            edges[edge].insert(triangle);
        }
    }
    
    for (map<int, set<const Triangle*> >::iterator edgeIterator = edges.begin();
        edgeIterator != edges.end();
        edgeIterator++) {
        int edge = edgeIterator->first;
        set<const Triangle*>& neighbors = edgeIterator->second;
        
        for (set<const Triangle*>::iterator outerIterator = neighbors.begin();
            outerIterator != neighbors.end();
            outerIterator++) { 
            const Triangle* outer = *outerIterator;
            
            for (set<const Triangle*>::iterator innerIterator = neighbors.begin();
            innerIterator != neighbors.end();
            innerIterator++) { 
                const Triangle* inner = *innerIterator;
                
                if (inner == outer) {
                    continue;
                }
                
                map<int, const Triangle*>& outerNeighbors = m_neighbors[outer];
                assert(outerNeighbors.find(edge) == outerNeighbors.end());
                outerNeighbors[edge] = inner;
            }
        }
    }
}

const vector<Mesh*>& Model::getMeshes() const {
    return m_meshes;
}

const Mesh& Model::getMesh(unsigned int index) const {
    assert(index >= 0 && index < m_meshes.size());
    return *(m_meshes[index]);
}

const vector<Triangle*>& Model::getTriangles() const {
    return m_triangles;
}

const Triangle& Model::getTriangle(unsigned int index) const {
    assert(index >= 0 && index < m_triangles.size());
    return *(m_triangles[index]);
}

const map<int, const Triangle*>& Model::getNeighbors(const Triangle* triangle) const {
    assert(m_neighbors.find(triangle) != m_neighbors.end());
    return m_neighbors.find(triangle)->second;
}

const Vertex& Model::getVertex(unsigned int index) const {
    assert(index >= 0 && index < m_vertices.size());
    return *(m_vertices[index]);
}

const Material& Model::getMaterial(unsigned int index) const {
    assert(index >= 0 && index < m_materials.size());
    return *(m_materials[index]);
}

const Matrix& Model::getMatrix() const {
    return m_matrix;
}

void Model::setMatrix(const Matrix& matrix) {
    m_matrix = matrix;
}

void Model::replacePainter(Painter* painter) {
    if (m_painter) {
        delete m_painter;
    }

    m_painter = painter;
}

void Model::draw(const Vector& eye, const Vector& light) const 
{
    Vector inverseLight = light.multiplyLeft(m_matrix.invert());

    glPushMatrix();
    glMultMatrixf(m_matrix.toArray());

    m_painter->paintModel(*this, eye, inverseLight);

    glPopMatrix();
}

ostream& operator<< (ostream& out, const Model& model)
{
    for (vector<Mesh*>::const_iterator iter = model.m_meshes.begin();
        iter != model.m_meshes.end();
        iter++) {
        out << "#Mesh START" << endl;
        out << *(*iter);
        out << "#Mesh END" << endl;
    }

    return out;
}
