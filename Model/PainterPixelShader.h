
#ifndef __PAINTERPIXELSHADER_H
#define __PAINTERPIXELSHADER_H

#include <Cg/cg.h>
#include <Cg/cgGL.h>

#include "Vector.h"

#include "Mesh.h"
#include "Triangle.h"

#include "Painter.h"

/**
    @file PainterPixelShader.h
    @brief Declares the PainterPixelShader class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief Implements the Painter visitor to draw a Model with detail and bumpmapping
    on the screen.
    
    The drawing process uses the pixel shader facilities of the card in order to render
    the detail and bumpmap layers.
*/
class PainterPixelShader: public Painter {
private:
    /** The CgProfile in which the #m_CgProgram will execute. */
    const CGprofile& m_CgProfile;
    /** The CgProgram which renders the fragments. */
    const CGprogram& m_CgProgram;

public:
    /** @brief Initialize the object with the given Cg objects. */
    PainterPixelShader(const CGprofile& cgProfile, const CGprogram& cgProgram);

    /** 
        @brief Draw the Mesh using pixel shaders.

        This method uses the pixel shader facilities of the card:

        <ol>
            <li>Bind the detail texture to the first texture unit.</li>
            <li>Bind the normalmap texture to the second texture unit.</li>
            <li>Bind the ambient and specular colors to uniform Cg parameters.</li>
            <li>Draw each Triangle fragment using the pixel shader.</li>
        </ol>
    */
    virtual void paintMesh(const Mesh& mesh, const Vector& eye, const Vector& light) const;

    /**
        @brief Draw the Triangle using pixel shaders.

        This method uses the pixel shader facilities of the card:

        <ol>
            <li>Bring the light into texture space and bind it to the first
            color parameter for the vertex.</li>
            <li>Bring the half vector into texture space and bind it to the
            second color parameter for the vertex.</li>
            <li>Bind the texture coordinates to the first and second texture
            unit coordinates.</li>
        </ol>

        The pixel shader program uses these parameters and the global state
        setup in #paintMesh to render the Triangle.
    */
    virtual void paintTriangle(const Triangle& triangle, const Vector& eye, const Vector& light) const;
};

#endif // __PAINTERPIXELSHADER_H
