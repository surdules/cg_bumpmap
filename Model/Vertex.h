
#ifndef __VERTEX_H
#define __VERTEX_H

#include "MS3D.h"

#include "Vector.h"

/**
    @file Vertex.h
    @brief Declares the Vertex class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief Defines a Vertex used in a
    <a href="http://www.swissquake.ch/chumbalum-soft/ms3d/">Milkshape3D</a> Model.
*/
class Vertex {
private:
    /** @brief The bone ID corresponding to this Vertex (unused). */
    char m_boneID;
    /** @brief The coordinates of this Vertex. */
    Vector m_location;

private:
    /** @brief Do not allow this Vertex to be copied. */
    Vertex(const Vertex& copy);
    /** @brief Do not allow this Vertex to be copied. */
    Vertex& operator=(const Vertex& rhs);

public:
    /** 
        @brief Initialize the Triangle using data from the corresponding
        Milkshape vertex.
    */
    Vertex(const MS3DVertex* const vertex);
    
    /** @brief Retrieve the location of this Vertex. */
    const Vector& getLocation() const;
};

#endif // __VERTEX_H
