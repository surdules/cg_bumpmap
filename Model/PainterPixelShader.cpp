
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <GL/gl.h>

#include <iostream>

#include "GLproc.h"

#include "PainterPixelShader.h"

using namespace std;

PainterPixelShader::PainterPixelShader(const CGprofile& cgProfile, const CGprogram& cgProgram):
    m_CgProfile(cgProfile), m_CgProgram(cgProgram)
{
    // nop
}

void PainterPixelShader::paintMesh(const Mesh& mesh, const Vector& eye, const Vector& light) const {
    if (!mesh.hasMaterial()) {
        return;
    }

    const Material& material = mesh.getMaterial();

    if (!material.hasDetail() || !material.hasBumpmap()) {
        cerr << "PainterPixelShader::paintMesh(): material does not have detail or bumpmap layer" << endl;
        exit(1);
    }

    glPushAttrib(GL_CURRENT_BIT | GL_LIGHTING_BIT | GL_TEXTURE_BIT);

    // OpenGL lighting must be disabled since the pixel shader
    // program will compute the lighting value
    glDisable(GL_LIGHTING);

    // The first texture unit contains the detail texture
    glActiveTextureARB(GL_TEXTURE0_ARB);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, material.getDetailHandle());

    // The second texture unit contains the normalmap texture
    glActiveTextureARB(GL_TEXTURE1_ARB);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, material.getNormalmapHandle());

    // Use the Cg fragment program to compute the colors
    cgGLBindProgram(m_CgProgram);
    cgGLEnableProfile(m_CgProfile);

    // Set the (fixed) ambient color value
    CGparameter ambientColorParameter = cgGetNamedParameter(m_CgProgram, "ambientColor");
    cgGLSetParameter3fv(ambientColorParameter, material.getAmbientColor().toArray());

    // Set the (fixed) specular color value
    CGparameter specularColorParameter = cgGetNamedParameter(m_CgProgram, "specularColor");
    cgGLSetParameter3fv(specularColorParameter, material.getSpecularColor().toArray());

    Vector inverseEye = eye.multiplyLeft(mesh.getModel().getMatrix().invert());

    const vector<const Triangle*>& triangles = mesh.getTriangles();
    for (vector<const Triangle*>::const_iterator iter = triangles.begin();
        iter != triangles.end();
        iter++) {
        const Triangle* triangle = *iter;
        paintTriangle(*triangle, inverseEye, light);
    }

    cgGLDisableProfile(m_CgProfile);

    glPopAttrib();
}

void PainterPixelShader::paintTriangle(const Triangle& triangle, const Vector& eye, const Vector& light) const {
    glBegin(GL_TRIANGLES);

    for (int i = 0; i < 3; i++) {
        const Vector& location = triangle.getVertex(i).getLocation();

        // Bring the light vector into tangent space
        Vector vertexToLight = light.subtract(location).multiplyLeft(
                triangle.getObjectToTangentSpaceMatrix(i)).normalize().rangeCompress();

        // Bind the light vector to COLOR0 and interpolate
        // it across the edge
        glColor3fv(vertexToLight.toArray());

        // Compute the half vector and bring it into
        // tangent space
        Vector view = eye.subtract(location);
        Vector half = light.add(view).multiplyLeft(
            triangle.getObjectToTangentSpaceMatrix(i)).normalize().rangeCompress();

        // Bind the half vector to COLOR1 and interpolate
        // it across the edge
        glSecondaryColor3fvEXT(half.toArray());

        const Vector& texture = triangle.getTextureCoordinates(i);

        // Bind the texture coordinates to TEXTURE0 and
        // interpolate them across the edge
        glMultiTexCoord2fARB(GL_TEXTURE0_ARB,
            texture[Vector::X], texture[Vector::Y]);

        // Bind the normalmap coordinates to TEXTURE1 and
        // interpolate them across the edge
        glMultiTexCoord2fARB(GL_TEXTURE1_ARB,
            texture[Vector::X], texture[Vector::Y]);

        glVertex3fv(location.toArray());
    }

    glEnd();
}
