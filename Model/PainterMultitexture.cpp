
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <GL/gl.h>

#include <iostream>

#include "GLproc.h"

#include "PainterMultitexture.h"

using namespace std;

PainterMultitexture::PainterMultitexture():
    m_perturbTextureCoordinates(false)
{
    // nop
}

void PainterMultitexture::paintMesh(const Mesh& mesh, const Vector& eye, const Vector& light) const {
    if (!mesh.hasMaterial()) {
        return;
    }

    const Material& material = mesh.getMaterial();

    if (!material.hasDetail() || !material.hasBumpmap()) {
        cerr << "PainterMultitexture::paintMesh(): material does not have detail or bumpmap layer" << endl;
        exit(1);
    }

    glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_LIGHTING_BIT | GL_TEXTURE_BIT);

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material.getAmbientColor().toArray());
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material.getDiffuseColor().toArray());
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material.getSpecularColor().toArray());
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, material.getEmissiveColor().toArray());
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, material.getSpecularShininess());

    // === First Pass ===

    // Turn off the light during the bumpmap step
    glDisable(GL_LIGHTING);

    // The first texture unit contains the bumpmap
    glActiveTextureARB(GL_TEXTURE0_ARB);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, material.getBumpmapHandle());
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT);
    glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE);
    
    // The second texture unit contains the inverted bumpmap
    glActiveTextureARB(GL_TEXTURE1_ARB);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, material.getInvertedBumpmapHandle());
    // Add the inverted bumpmap to the bumpmap (effectively subtracts them)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT);
    glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_ADD);

    m_perturbTextureCoordinates = true;
    const vector<const Triangle*>& triangles = mesh.getTriangles();
    for (vector<const Triangle*>::const_iterator iter = triangles.begin();
        iter != triangles.end();
        iter++) {
        const Triangle* triangle = *iter;
        paintTriangle(*triangle, eye, light);
    }
    m_perturbTextureCoordinates = false;

    // === Second Pass ===

    // Draw only where we've drawn before
    glDepthFunc(GL_EQUAL);

    // Disable the second texture unit
    glActiveTextureARB(GL_TEXTURE1_ARB);
    glDisable(GL_TEXTURE_2D);

    // The first texture unit contains the detail
    glActiveTextureARB(GL_TEXTURE0_ARB);
    glBindTexture(GL_TEXTURE_2D, material.getDetailHandle());
    // Set the texture combiner to GL_MODULATE for correct lighting
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    
    // Blending(GL_DST_COLOR, GL_SRC_COLOR) multiplies by 2:
    //   (Cdst * Csrc) + (Csrc * Cdst) = 2 (Csrc * Cdst)
    // This is why the bumpmap texture is scaled by 50%
    glBlendFunc(GL_DST_COLOR,GL_SRC_COLOR);
    glEnable(GL_BLEND);

    glEnable(GL_LIGHTING);

    for (vector<const Triangle*>::const_iterator iter = triangles.begin(); iter != triangles.end(); iter++) {
        const Triangle* triangle = *iter;
        paintTriangle(*triangle, eye, light);
    }

    glPopAttrib();
}

void PainterMultitexture::paintTriangle(const Triangle& triangle, const Vector& eye, const Vector& light) const {
    float strength = 0.01f;

    glBegin(GL_TRIANGLES);

    for (int i = 0; i < 3; i++) {
        const Vector& location = triangle.getVertex(i).getLocation();

        const Vector& normal = triangle.getNormal(i);
        glNormal3fv(normal.toArray());

        const Vector& texture = triangle.getTextureCoordinates(i);
        if (m_perturbTextureCoordinates) {
            // Bring the light vector into tangent space
            // Once the light vector has been brought into texture
            // space, its X and Y coordinates represent the amount
            // of perturbation that should be applied to the s- and 
            // t- coordinates.
            Vector vertexToLight = light.subtract(location).multiplyLeft(
                triangle.getObjectToTangentSpaceMatrix(i)).normalize();

            glMultiTexCoord2fARB(GL_TEXTURE0_ARB, 
                texture[Vector::X], texture[Vector::Y]);

            glMultiTexCoord2fARB(GL_TEXTURE1_ARB, 
                texture[Vector::X] + vertexToLight[Vector::X] * strength, 
                texture[Vector::Y] + vertexToLight[Vector::Y] * strength);
        } else {
            glMultiTexCoord2fARB(GL_TEXTURE0_ARB, 
                texture[Vector::X], texture[Vector::Y]);
        }

        glVertex3fv(location.toArray());
    }

    glEnd();
}
