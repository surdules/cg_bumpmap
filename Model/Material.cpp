
#include <assert.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include <LodePng.h>

#include "Vector.h"

#include "Material.h"

Material::Material(const MS3DMaterial* material) :
    m_ambient(material->m_ambient[Color::R],
        material->m_ambient[Color::G],
        material->m_ambient[Color::B],
        material->m_ambient[Color::A]),
    m_diffuse(
        material->m_diffuse[Color::R],
        material->m_diffuse[Color::G],
        material->m_diffuse[Color::B],
        material->m_diffuse[Color::A]),
    m_specular(
        material->m_specular[Color::R],
        material->m_specular[Color::G],
        material->m_specular[Color::B],
        material->m_specular[Color::A]),
    m_emissive(
        material->m_emissive[Color::R],
        material->m_emissive[Color::G],
        material->m_emissive[Color::B],
        material->m_emissive[Color::A]),
    m_shininess(material->m_shininess),
    m_detailFileName(material->m_texture),
    m_bumpmapFileName(material->m_alphamap)
{
    m_hasDetail = !m_detailFileName.empty();
    m_hasBumpmap = !m_bumpmapFileName.empty();

    assert(m_hasDetail || !m_hasBumpmap);

    if (m_hasDetail) {
		std::vector<unsigned char> detailTexture;
		unsigned detailTextureWidth, detailTextureHeight;
		unsigned detailTextureError = lodepng::decode(
			detailTexture, detailTextureWidth, detailTextureHeight, m_detailFileName.c_str(), LCT_RGB);
		// TODO: flip_x
		assert(!detailTextureError);
		Array3<unsigned char> detailData(
			detailTextureWidth, detailTextureHeight, 3, (unsigned char*)detailTexture.data());

        initializeDetail(detailData);

        if (m_hasBumpmap) {
			std::vector<unsigned char> bumpmapTexture;
			unsigned bumpmapTextureWidth, bumpmapTextureHeight;
			unsigned bumpmapTextureError = lodepng::decode(
				bumpmapTexture, bumpmapTextureWidth, bumpmapTextureHeight, m_bumpmapFileName.c_str(), LCT_RGB);
			// TODO: flip_x
			assert(!bumpmapTextureError);
            Array3<unsigned char> bumpmapData(
				bumpmapTextureWidth, bumpmapTextureHeight, 3, (unsigned char*) bumpmapTexture.data());
            initializeBumpmap(bumpmapData);
            initializeNormalmap(bumpmapData);
        }
    }
}

void Material::initializeDetail(const Array3<unsigned char>& detailData)
{
    glPushAttrib(GL_TEXTURE_BIT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Generate the texture
    glGenTextures(1, &m_detailHandle);
    glBindTexture(GL_TEXTURE_2D, m_detailHandle);

    gluBuild2DMipmaps(GL_TEXTURE_2D,
        GL_RGB8,
        detailData.getWidth(),
        detailData.getHeight(),
        GL_RGB,
        GL_UNSIGNED_BYTE,
        detailData.getData());

    glPopAttrib();
}

void Material::initializeBumpmap(const Array3<unsigned char>& bumpmapData)
{
    glPushAttrib(GL_TEXTURE_BIT);

    // Scale luminance 50% because blending will double the intensity:
    // glBlend(GL_DST_COLOR, GL_SRC_COLOR)
    glPixelTransferf(GL_RED_SCALE, 0.5f);
    glPixelTransferf(GL_GREEN_SCALE, 0.5f);
    glPixelTransferf(GL_BLUE_SCALE, 0.5f);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    // Generate the bumpmap
    glGenTextures(1, &m_bumpmapHandle);
    glBindTexture(GL_TEXTURE_2D, m_bumpmapHandle);

    gluBuild2DMipmaps(GL_TEXTURE_2D,
        GL_RGB8,
        bumpmapData.getWidth(),
        bumpmapData.getHeight(),
        GL_RGB,
        GL_UNSIGNED_BYTE,
        bumpmapData.getData());

    // Generate the inverted bumpmap
    glGenTextures(1, &m_invertedBumpmapHandle);
    glBindTexture(GL_TEXTURE_2D, m_invertedBumpmapHandle);

    // NOTE: luminance still at 50%
    Array3<unsigned char> invertedBumpmapData(bumpmapData.getWidth(), bumpmapData.getHeight(), 3);
    for (int i = 0; i < bumpmapData.getWidth(); i++) {
        for (int j = 0; j < bumpmapData.getHeight(); j++) {
            invertedBumpmapData(i, j, 0) = 255 - bumpmapData(i, j, 0);
            invertedBumpmapData(i, j, 1) = 255 - bumpmapData(i, j, 1);
            invertedBumpmapData(i, j, 2) = 255 - bumpmapData(i, j, 2);
        }
    }

    gluBuild2DMipmaps(GL_TEXTURE_2D,
        GL_RGB8,
        invertedBumpmapData.getWidth(),
        invertedBumpmapData.getHeight(),
        GL_RGB,
        GL_UNSIGNED_BYTE,
        invertedBumpmapData.getData());

    // Scale luminance back to 100%
    glPixelTransferf(GL_RED_SCALE, 1.0f);
    glPixelTransferf(GL_GREEN_SCALE, 1.0f);
    glPixelTransferf(GL_BLUE_SCALE, 1.0f);

    glPopAttrib();
}

void Material::initializeNormalmap(const Array3<unsigned char>& bumpmapData)
{
    float strength = 0.01f;
    
    glPushAttrib(GL_TEXTURE_BIT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Generate the normalmap
    glGenTextures(1, &m_normalmapHandle);
    glBindTexture(GL_TEXTURE_2D, m_normalmapHandle);

    // Compute the center of the normalmapData (skip the edges
    // since we can't readily compute slopes at the edge).
    Array3<unsigned char> normalmapData(bumpmapData.getWidth(), bumpmapData.getHeight(), 3);
	for (int i = 1; i < bumpmapData.getWidth() - 1; i++) {
        for (int j = 1; j < bumpmapData.getHeight() - 1; j++) {
            Vector s(1, 0, strength * (bumpmapData(i + 1, j, 0) - bumpmapData(i - 1, j, 0)));
            Vector t(0, 1, strength * (bumpmapData(i, j + 1, 0) - bumpmapData(i, j - 1, 0)));
            Vector n = s.cross(t).normalize().rangeCompress(255);

            normalmapData(i, j, 0) = (GLbyte) n[Vector::X];
            normalmapData(i, j, 1) = (GLbyte) n[Vector::Y];
            normalmapData(i, j, 2) = (GLbyte) n[Vector::Z];
        }
    }

    // Fill in the top and bottom edge
    for (int i = 0; i < bumpmapData.getWidth(); i++) {
        normalmapData(i, 0, 0) = normalmapData(i, 1, 0);
        normalmapData(i, 0, 1) = normalmapData(i, 1, 1);
        normalmapData(i, 0, 2) = normalmapData(i, 1, 2);

        int last = normalmapData.getHeight() - 1;
        normalmapData(i, last, 0) = normalmapData(i, last - 1, 0);
        normalmapData(i, last, 1) = normalmapData(i, last - 1, 1);
        normalmapData(i, last, 2) = normalmapData(i, last - 1, 2);
    }

    // Fill in the left and right edge
    for (int i = 0; i < bumpmapData.getHeight(); i++) {
        normalmapData(0, i, 0) = normalmapData(1, i, 0);
        normalmapData(0, i, 1) = normalmapData(1, i, 1);
        normalmapData(0, i, 2) = normalmapData(1, i, 2);

        int last = normalmapData.getWidth() - 1;
        normalmapData(last, i, 0) = normalmapData(last - 1, i, 0);
        normalmapData(last, i, 1) = normalmapData(last - 1, i, 1);
        normalmapData(last, i, 2) = normalmapData(last - 1, i, 2);
    }

    gluBuild2DMipmaps(GL_TEXTURE_2D,
        GL_RGB8,
        normalmapData.getWidth(),
        normalmapData.getHeight(),
        GL_RGB,
        GL_UNSIGNED_BYTE,
        normalmapData.getData());

    glPopAttrib();
}

const Color& Material::getAmbientColor() const {
    return m_ambient;
}

const Color& Material::getDiffuseColor() const {
    return m_diffuse;
}

const Color& Material::getSpecularColor() const {
    return m_specular;
}

const Color& Material::getEmissiveColor() const {
    return m_emissive;
}

float Material::getSpecularShininess() const {
    return m_shininess;
}

bool Material::hasDetail() const {
    return m_hasDetail;
}

bool Material::hasBumpmap() const {
    return m_hasBumpmap;
}

unsigned int Material::getDetailHandle() const {
    return m_detailHandle;
}

unsigned int Material::getBumpmapHandle() const {
    return m_bumpmapHandle;
}

unsigned int Material::getInvertedBumpmapHandle() const {
    return m_invertedBumpmapHandle;
}

unsigned int Material::getNormalmapHandle() const {
    return m_normalmapHandle;
}
