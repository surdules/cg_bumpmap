
#ifndef __TRIANGLE_H
#define __TRIANGLE_H

#include <ostream>
#include <vector>

#include "MS3D.h"

#include "Vector.h"
#include "Matrix.h"

#include "Vertex.h"

class Model;

/**
    @file Triangle.h
    @brief Declares the Triangle class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief Defines a Triangle used in a
    <a href="http://www.swissquake.ch/chumbalum-soft/ms3d/">Milkshape3D</a> Model.
*/
class Triangle {
protected:
    /** @brief The Vertex objects which define this Triangle. */
    std::vector<const Vertex*> m_vertices;
    /** @brief The Triangle normal. */
    Vector m_normal;
    /** @brief The normals at each Vertex. */
    std::vector<Vector> m_vertexNormals;
    /** @brief The texture coordinates at each Vertex. */
    std::vector<Vector> m_textureCoordinates;
    /**
        @brief The edge indices for this Triangle.

        The edge index is defined as the combination of the two Vertex
        indices for the two ends of that edge. For example, say this Triangle
        has Vertex indices 0xA3, 0x17 and 0xF8. The first edge is made of
        Vertices (0xA3, 0x17), the second edge (0x17, 0xF8) and the third edge
        (0xF8, 0xA3). The edge index of the first edge is 0xA317, the second
        edge is 0x17F8 and the third edge is 0xF8A3.

        Each edge index is therefore guaranteed to be shared between two
        Triangles if they share the corresponding Vertices. Furthermore, since
        we know that all Triangles vertices are listed in counter-clockwise
        order, the edge indices will always be created in the appropriate order.
    */
    std::vector<int> m_edgeIndices;

    /** 
        @brief Per vertex transform from object to tangent space. 

        This matrix is used to bring the light Vector from object
        space into tangent space.

        "Tangent space" is the vector space defined by the s-, t-
        and (s- cross t-) texture directions. The (s- cross t-)
        direction "corresponds" to the triangle normal (in "object
        space").

        Given a point P inside the face (with texture coordinates
        s_P and t_P), and a vertex E belonging to the face (with 
        texture coordinates s_E and t_E) we would like for the 
        following to hold:
        <pre>
        P - E = (s_P - s_E) * T + (t_P - t_E) * B
        </pre>
        where (T, B) represent the s- and, respectively, t- 
        directions in texture space.

        Let V1V0 and V2V0 be two edges of the face; using the
        relationship above:
        <pre>
        V1V0 = (s_V1 - s_V0) * T + (t_V1 - t_V0) * B
        V2V0 = (s_V2 - s_V0) * T + (t_V2 - t_V0) * B
        </pre>

        Let s1 = s_V1 - s_V0, t1 = t_V1 - t_V0,
        s2 = s_V2 - s_V0, t2 = t_V2 - t_V0

        Solving for T, B in the system of equations above, we get:
        <pre>
        |T| = 1 / (s1 * t2 - s2 * t2) |t2 -t1| |V1V0|
        |B|                           |-s2 s1| |V2V0|
        </pre>

        The matrix which transforms from texture space to object
        space has as columns the vectors T, B and N = T cross B. This 
        can be verified immediately: the matrix takes the the X axis 
        (1,0,0) in texture space to T in object space.

        To transform from object space to texture space, we need
        to invert the matrix. This will take the normal in object
        space to the Z axis (0, 0, 1) in texture space.
    */
    std::vector<Matrix> m_objectToTangentSpace;

    /** @brief The Model to which this Triangle belongs. */
    const Model& m_model;
    
private:
    /** @brief Do not allow this Triangle to be copied. */
    Triangle(const Triangle& copy);
    /** @brief Do not allow this Triangle to be copied. */
    Triangle& operator=(const Triangle& rhs);

public:
    /** 
        @brief Initialize the Triangle using data from the corresponding
        Milkshape triangle.
    */
    Triangle(const MS3DTriangle* const triangle, const Model& model);

    /** @brief Retrieve the given Vertex for this Triangle. */
    const Vertex& getVertex(unsigned int which) const;
    /** @brief Retrieve the normal of the Triangle. */
    const Vector& getNormal() const;
    /** @brief Retrieve the normal for the given Vertex of this Triangle. */
    const Vector& getNormal(unsigned int which) const;
    /** 
        @brief Retrieve the texture coordinates at the given Vertex for 
        this Triangle.
    */
    const Vector& getTextureCoordinates(unsigned int which) const;
    /** 
        @brief Retrieve the matrix which converts from object to tangent space 
        at the given Vertex for this Triangle.
    */
    const Matrix& getObjectToTangentSpaceMatrix(unsigned int which) const;
    /**
        @brief Retrieve the edge index for the given edge for this Triangle.
    */
    int getEdgeIndex(unsigned int which) const;

    /**
        @brief Is this Triangle facing the light?

        The determination is made by computing the angle between the Triangle's
        Normal and the direction from the light to one of the Triangle's
        Vertices. If this angle is between 180 and 360, it is facing away
        otherwise it is facing towards.
    */
    bool isFacing(const Vector& point) const;
    
    /**
        @brief Insertion operator for this Triangle into an output
        stream.
    */
    friend std::ostream& operator<< (std::ostream& out, const Triangle& triangle);
};

#endif // __TRIANGLE_H
