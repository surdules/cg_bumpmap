
#include <assert.h>

#include "Const.h"

#include "Model.h"

#include "Triangle.h"

using namespace std;

Triangle::Triangle(const MS3DTriangle* const triangle, const Model& model) : 
    m_model(model)
{
    for (int i = 0; i < 3; i++) {
        const Vertex& vertex = m_model.getVertex(triangle->m_vertexIndices[i]);
        m_vertices.push_back(&vertex);

        m_vertexNormals.push_back(Vector(
            triangle->m_vertexNormals[i][Vector::X],
            triangle->m_vertexNormals[i][Vector::Y],
            triangle->m_vertexNormals[i][Vector::Z]));

        m_textureCoordinates.push_back(Vector(
            triangle->m_s[i], 1.0f - triangle->m_t[i], 0));
    }

    for (int i = 0; i < 3; i++) {
        int edge;
    
        int firstIndex = triangle->m_vertexIndices[i];
        int secondIndex = triangle->m_vertexIndices[(i + 1) % 3];
        if (firstIndex < secondIndex) {
            edge = (firstIndex << 16) | secondIndex;
        } else {
            edge = (secondIndex << 16) | firstIndex;
        }

        m_edgeIndices.push_back(edge);
    }

    Vector v1 = m_vertices[1]->getLocation().subtract(m_vertices[0]->getLocation());
    Vector v2 = m_vertices[2]->getLocation().subtract(m_vertices[0]->getLocation());
    m_normal = v1.cross(v2).normalize();

    for (int currentIndex = 0; currentIndex < 3; currentIndex++) {
        int previousIndex = (currentIndex == 0 ? 2 : currentIndex - 1);
        int nextIndex = (currentIndex == 2 ? 0 : currentIndex + 1);

        const Vertex& current = getVertex(currentIndex);
        const Vertex& previous = getVertex(previousIndex);
        const Vertex& next = getVertex(nextIndex);
        
        Vector v1v0 = next.getLocation().subtract(current.getLocation());
        Vector v2v0 = previous.getLocation().subtract(current.getLocation());
        
        float s1 = m_textureCoordinates[nextIndex][Vector::X] - 
            m_textureCoordinates[currentIndex][Vector::X];
        float t1 = m_textureCoordinates[nextIndex][Vector::Y] - 
            m_textureCoordinates[currentIndex][Vector::Y];
        
        float s2 = m_textureCoordinates[previousIndex][Vector::X] - 
            m_textureCoordinates[currentIndex][Vector::X];
        float t2 = m_textureCoordinates[previousIndex][Vector::Y] - 
            m_textureCoordinates[currentIndex][Vector::Y];

        float denominator = (s1 * t2 - s2 * t1);
        if (denominator < EPSILON) {
            // If we cannot solve for t and b, use the IDENTITY Matrix
            m_objectToTangentSpace.push_back(Matrix::IDENTITY);
        } else {
            float scale = 1.0f / denominator;
        
            Vector t = Vector(
                (t2 * v1v0[Vector::X] - t1 * v2v0[Vector::X]) / scale,
                (t2 * v1v0[Vector::Y] - t1 * v2v0[Vector::Y]) / scale,
                (t2 * v1v0[Vector::Z] - t1 * v2v0[Vector::Z]) / scale).normalize();
        
            Vector b = Vector(
                (-s2 * v1v0[Vector::X] + s1 * v2v0[Vector::X]) / scale,
                (-s2 * v1v0[Vector::Y] + s1 * v2v0[Vector::Y]) / scale,
                (-s2 * v1v0[Vector::Z] + s1 * v2v0[Vector::Z]) / scale).normalize();

            m_objectToTangentSpace.push_back(Matrix(t, b, t.cross(b)).invert());
        }
    }
}

const Vertex& Triangle::getVertex(unsigned int which) const {
    assert(which >= 0 && which <= 2);
    return *(m_vertices[which]);
}

const Vector& Triangle::getNormal() const {
    return m_normal;
}

const Vector& Triangle::getNormal(unsigned int which) const {
    assert(which >= 0 && which <= 2);
    return m_vertexNormals[which];
}

const Vector& Triangle::getTextureCoordinates(unsigned int which) const {
    assert(which >= 0 && which <= 2);
    return m_textureCoordinates[which];
}

const Matrix& Triangle::getObjectToTangentSpaceMatrix(unsigned int which) const {
    assert(which >= 0 && which <= 2);
    return m_objectToTangentSpace[which];
}

int Triangle::getEdgeIndex(unsigned int which) const {
    assert(which >= 0 && which <= 2);
    return m_edgeIndices[which];
}

bool Triangle::isFacing(const Vector& point) const {
    Vector direction = point.subtract(m_vertices[0]->getLocation());
    if (direction.dot(m_normal) <= 0) {
        return false;
    } else {
        return true;
    }
}

ostream& operator<< (ostream& out, const Triangle& triangle)
{
    for (int i = 0; i < 3; i++) {
        const Vertex& vertex = triangle.getVertex(i);
        const Vector& vector = vertex.getLocation();
        out << vector[Vector::X] << " " << vector[Vector::Y] << " " << vector[Vector::Z] << " ";
        out << triangle.m_textureCoordinates[i][Vector::X] << " ";
        out << triangle.m_textureCoordinates[i][Vector::Y] << endl;
    }

    return out;
}
