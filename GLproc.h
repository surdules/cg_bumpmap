
#ifndef __GLPROC_H
#define __GLPROC_H

/**
    @file GLproc.h
    @brief Declares the extern function pointers corresponding to OpenGL extensions.

    The PFN* types are pre-defined datatypes (from glext.h)

    Since we only use the GLfloat-version, we only need prototypes for the 
    commands ending with an "f". Others are also available ("fv", "i", etc.) 
    ARB is an abbreviation for "Architectural Review Board". Extensions with 
    ARB in their name are not required by an OpenGL-conformant implementation, 
    but they are expected to be widely supported.

    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <GL/gl.h>
#include <GL/glext.h>

/**
    @brief Function pointer for specifying multitexture coordinates on the
    first multitexture unit.

    The commands glMultiTexCoordifARB map to the well-known glTexCoordif, 
    specifying i-dimensional texture-coordinates. These can substitute the 
    glTexCoordif functions. 
*/
extern PFNGLMULTITEXCOORD1FARBPROC glMultiTexCoord1fARB;

/**
    @brief Function pointer for specifying multitexture coordinates on the
    second multitexture unit.

    The commands glMultiTexCoordifARB map to the well-known glTexCoordif, 
    specifying i-dimensional texture-coordinates. These can substitute the 
    glTexCoordif functions. 
*/
extern PFNGLMULTITEXCOORD2FARBPROC glMultiTexCoord2fARB;

/**
    @brief Function pointer for specifying multitexture coordinates on the
    third multitexture unit.

    The commands glMultiTexCoordifARB map to the well-known glTexCoordif, 
    specifying i-dimensional texture-coordinates. These can substitute the 
    glTexCoordif functions. 
*/
extern PFNGLMULTITEXCOORD3FARBPROC glMultiTexCoord3fARB;

/**
    @brief Function pointer for specifying multitexture coordinates on the
    fourth multitexture unit.

    The commands glMultiTexCoordifARB map to the well-known glTexCoordif, 
    specifying i-dimensional texture-coordinates. These can substitute the 
    glTexCoordif functions. 
*/
extern PFNGLMULTITEXCOORD4FARBPROC glMultiTexCoord4fARB;

/** @brief Function pointer for selecting the active multitexture unit. */
extern PFNGLACTIVETEXTUREARBPROC glActiveTextureARB;

/** @brief Function pointer for selecting the active multitexture coordinate array. */
extern PFNGLCLIENTACTIVETEXTUREARBPROC glClientActiveTextureARB;

/** @brief Function pointer for setting the secondary color. */
extern PFNGLSECONDARYCOLOR3FEXTPROC glSecondaryColor3fEXT;

/** @brief Function pointer for setting the secondary color. */
extern PFNGLSECONDARYCOLOR3FVEXTPROC glSecondaryColor3fvEXT;

/** @brief Initialize the function pointers to their implementation. */
void initExtensions();

#endif // __GLPROC_H
