
#ifndef __TRANSFORMERPAN_H
#define __TRANSFORMERPAN_H

#include "Matrix.h"

#include "Transformer.h"

/**
    @file TransformerPan.h
    @brief Declares the TransformerPan class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief Implements the Transformer to compute the transformation Matrix as 
    a translation left/right and up/down on the screen (along the X and, respectively, 
    Y axis) from the current mouse position in the direction of the mouse movement.
*/
class TransformerPan: public Transformer {
private:
    /** @brief The mouse click starting coordinate. */
    float m_startX, m_startY;
    /** 
        @brief The scaling factor for the mouse movement. 

        The mouse movement distance (in window space) is scaled by this value 
        in order to avoid creating translations which are too big or to small 
        relative to the object coordinates.
    */
    float m_scale;

public:
    /** @brief Create a TransformerPan with the given dimensions and scaling factor. */
    TransformerPan(int width, int height, float scale);

    /**
        @brief Update the TransformerPan dimensions.

        This method does nothing.
    */
    void reshape(int width, int height);

    /**
        @brief Starts a translation. 

        Sets up #m_startX and #m_startY to point to the current mouse click.
    */
    void start(float x, float y);

    /**
        @brief Compute the translation matrix determined by the start 
        of the transformation (#m_startX, #m_startY) and the current mouse
        position. 
    */
    const Matrix end(float x, float y);
};

#endif // __TRANSFORMERPAN_H
