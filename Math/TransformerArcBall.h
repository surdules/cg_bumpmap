
#ifndef __TRANSFORMERARCBALL_H
#define __TRANSFORMERARCBALL_H

#include "Vector.h"
#include "Matrix.h"
#include "Quaternion.h"

#include "Transformer.h"

/**
    @file TransformerArcBall.h
    @brief Declares the TransformerArcBall class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief Implements the Transformer to compute the transformation Matrix by 
    interpreting the mouse position as points on a hemisphere.
*/
class TransformerArcBall: public Transformer {
private:
    /** 
        @brief The center of the hemisphere on which the rotation is
        represented.

        The center and the #m_radius are guaranteed to define a hemisphere
        which covers the entire window.
    */
    float m_centerX, m_centerY;

    /** 
        @brief The radius of the hemisphere on which the rotation is
        represented.

        The radius and the (#m_centerX, #m_centerY) are guaranteed to define 
        a hemisphere which covers the entire window.
    */
    float m_radius;

    /**
        @brief The Vector from the center of the hemisphere to the point
        on the hemisphere defined by the current rotation.
    */
    Vector m_rotationVector;

private:
    /**
        @brief Computes the point on the hemisphere corresponding to
        a point on the screen.

        Since the hemisphere is guaranteed to cover the entire screen,
        we know that points on the screen will fall somewhere on the 
        sphere. The Z coordinate (the "height" of the point) is computed 
        trivially using Pythagora's theorem.
    */
    float sphereZ(float x, float y);

public:
    /** @brief Create an TransformerArcBall with the given dimensions. */
    TransformerArcBall(int width, int height);

    /**
        @brief Update the TransformerArcBall dimensions.

        The #m_centerX and #m_centerY variables are setup to 
        be in the middle of the screen (half of width and, respectively, 
        height). The #m_radius variable is setup to be the distance 
        from (#m_centerX, #m_centerY) to the corner of the screen 
        (thereby ensuring that the hemisphere covers the entire surface of 
        the screen).
    */
    void reshape(int width, int height);

    /**
        @brief Starts a rotation at the given screen coordinates.

        Sets up the #m_rotationVector to point to the point on the sphere 
        corresponding to the given position (x, y, #sphereZ())
    */
    void start(float x, float y);

    /**
        @brief Compute the rotation matrix determined by the start 
        of the rotation (#m_rotationVector) and the current screen position. 
        
        The current screen position is represented by the Vector
        (x, y, #sphereZ()).

        The rotation is computed as follows:

        <ul>
            <li>Compute the @link Vector#angle angle @endlink between 
            #m_rotationVector and the current Vector and multiply by 2.0. 
            This is the angle for the rotation.</li>
            <li>Compute and normalize the cross product between the 
            #m_rotationVector and the current Vector. This is the axis 
            around which the rotation is performed.</li>
            <li>Initialize and normalize the Quaternion representing
            the rotation described above (angle, axis).</li>
            <li>Return the Matrix corresponding to the Quaternion.</li>
        </ul>
    */
    const Matrix end(float x, float y);
};

#endif // __TRANSFORMERARCBALL_H
