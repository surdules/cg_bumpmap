
#ifndef __TRANSFORMERZOOM_H
#define __TRANSFORMERZOOM_H

#include "Matrix.h"

#include "Transformer.h"

/**
    @file TransformerZoom.h
    @brief Declares the TransformerZoom class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief Implements the Transformer to compute the transformation Matrix as 
    a translation in/out of the screen (along the Z axis) from the current 
    mouse position in the direction of the Y mouse movement.

    If the mouse moves up, the translation into the screen (positive Z axis), else
    the transformation is out of the screen (negative Z axis).
*/
class TransformerZoom: public Transformer {
private:
    /** @brief The mouse click starting Y coordinate. */
    float m_startY;
    /** 
        @brief The scaling factor for the mouse movement. 

        The mouse movement distance (in window space) is scaled by this value 
        in order to avoid creating translations which are too big or to small 
        relative to the object coordinates.
    */
    float m_scale;

public:
    /** @brief Create a TransformerZoom with the given dimensions and scaling factor. */
    TransformerZoom(int width, int height, float scale);

    /**
        @brief Update the TransformerZoom dimensions.

        This method does nothing.
    */
    void reshape(int width, int height);

    /**
        @brief Starts a translation. 

        Sets up #m_startY to point to the current mouse click.
    */
    void start(float x, float y);

    /**
        @brief Compute the translation matrix determined by the start 
        of the transformation (#m_startY) and the current mouse Y
        position. 
    */
    const Matrix end(float x, float y);
};

#endif // __TRANSFORMERZOOM_H
