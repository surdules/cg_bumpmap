
#ifndef __TRANSFORMER_H
#define __TRANSFORMER_H

#include "Matrix.h"

/**
    @file Transformer.h
    @brief Declares the Transformer class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief The Transformer is an interface specifying how to compute the transformation
    Matrix from an initial mouse click (screen position) and final mouse click.
*/
class Transformer {
public:
    /** 
        @brief Virtual (emtpy) destructor defined to ensure that derived classes' 
        destructors are invoked.
    */
    virtual ~Transformer() {
        // nop
    }

    /** @brief Update the Transformer screen dimensions. */
    virtual void reshape(int width, int height) = 0;

    /** @brief Starts a transformation at the given screen coordinates. */
    virtual void start(float x, float y) = 0;

    /** 
        @brief Computes the Matrix representing the transformation from
        the initial screen coordinates (#start) and the current screen
        coordinates.
    */
    virtual const Matrix end(float x, float y) = 0;
};

#endif // __TRANSFORMER_H
