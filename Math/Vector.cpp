
#include <assert.h>
#include <math.h>

#include "Const.h"

#include "Vector.h"
#include "Matrix.h"

using namespace std;

const int Vector::X = 0;
const int Vector::Y = 1;
const int Vector::Z = 2;
const int Vector::W = 3;

const Vector Vector::ORIGIN(0, 0, 0);

Vector::Vector()
{
    initialize(0.0, 0.0, 0.0, 1.0);
}

Vector::Vector(float x, float y, float z)
{
    initialize(x, y, z, 1.0);
}

Vector::Vector(float x, float y, float z, float w)
{
    initialize(x, y, z, w);
}

void Vector::initialize(float x, float y, float z, float w)
{
    m_data[X] = x;
    m_data[Y] = y;
    m_data[Z] = z;
    m_data[W] = w;
}

float& Vector::operator[](int which)
{
    assert(which >= X && which <= W);
    return m_data[which];
}

float Vector::operator[](int which) const
{
    assert(which >= X && which <= W);
    return m_data[which];
}

const float* Vector::toArray() const
{
    return m_data;
}

const Vector Vector::normalize() const
{
    float _length = length();
    assert(_length > EPSILON);

    return this->scale(1 / _length);
}

float Vector::length() const
{
    return (float) sqrt(
        m_data[X] * m_data[X] + 
        m_data[Y] * m_data[Y] + 
        m_data[Z] * m_data[Z]);
}

float Vector::dot(const Vector& vert) const
{
    return 
        m_data[X] * vert[X] + 
        m_data[Y] * vert[Y] + 
        m_data[Z] * vert[Z];
}

const Vector Vector::cross(const Vector& vert) const
{
    return Vector(
        m_data[Y] * vert[Z] - m_data[Z] * vert[Y],
        m_data[Z] * vert[X] - m_data[X] * vert[Z],
        m_data[X] * vert[Y] - m_data[Y] * vert[X]);
}

float Vector::angle(const Vector& vert) const
{
    float _dot = dot(vert);
    _dot = _dot / (length() * vert.length());
    assert(_dot >= -1 && _dot <= 1);

    return (float) acos(_dot);
}

const Vector Vector::add(const Vector& vert) const
{
    return Vector(
        m_data[X] + vert[X],
        m_data[Y] + vert[Y],
        m_data[Z] + vert[Z]);
}

const Vector Vector::subtract(const Vector& vert) const
{
    return Vector(
        m_data[X] - vert[X],
        m_data[Y] - vert[Y],
        m_data[Z] - vert[Z]);
}

const Vector Vector::scale(float scale) const
{
    return Vector(
        m_data[X] * scale,
        m_data[Y] * scale,
        m_data[Z] * scale);
}

const Vector Vector::multiplyLeft(const Matrix& matrix) const
{
    Vector result(0, 0, 0, 0);

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            result[row] += matrix(row, col) * m_data[col];
        }
    }

    for (int index = 0; index < 3; index++) {
        result[index] /= result[W];
    }
    result[W] = 1;

    return result;
}
    
const Vector Vector::rangeCompress(float scale) const
{
    return Vector(
        (m_data[X] + 1.0f) * scale / 2.0f,
        (m_data[Y] + 1.0f) * scale / 2.0f,
        (m_data[Z] + 1.0f) * scale / 2.0f);
}

ostream& operator<< (ostream& out, const Vector& vect)
{
    out << "[" << 
        vect[Vector::X] << "," << 
        vect[Vector::Y] << "," << 
        vect[Vector::Z] << "," << 
        vect[Vector::W] << "]";
    return out;
}
