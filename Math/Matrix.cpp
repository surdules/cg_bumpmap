
#include <assert.h>
#include <math.h>

#include "Const.h"

#include "Matrix.h"

using namespace std;

const Matrix Matrix::IDENTITY;

Matrix::Matrix()
{
    for (int i = 0; i < 16; i++) {
        if ((i % 5) == 0) {
            m_data[i] = 1.0f;
        } else {
            m_data[i] = 0.0f;
        }
    }
}

Matrix::Matrix(const float* const data) 
{
    initialize(data);
}

Matrix::Matrix(const Vector& col1, const Vector& col2, const Vector& col3)
{
	int i;
    for (i = 0; i < 3; i++) {
        m_data[i] = col1[i];
    }
    m_data[i] = 0;

    for (i = 0; i < 3; i++) {
        m_data[4 + i] = col2[i];
    }
    m_data[4 + i] = 0;

    for (i = 0; i < 3; i++) {
        m_data[8 + i] = col3[i];
    }
    m_data[8 + i] = 0;

    for (i = 0; i < 3; i++) {
        m_data[12 + i] = 0;
    }
    m_data[12 + i] = 1;
}

Matrix::Matrix(const Vector& col1, const Vector& col2, const Vector& col3, const Vector& col4)
{
    for (int i = 0; i < 4; i++) {
        m_data[i] = col1[i];
    }

    for (int i = 0; i < 4; i++) {
        m_data[4 + i] = col2[i];
    }

    for (int i = 0; i < 4; i++) {
        m_data[8 + i] = col3[i];
    }

    for (int i = 0; i < 4; i++) {
        m_data[12 + i] = col4[i];
    }
}

void Matrix::initialize(const float* const data)
{
    assert(data);

    for (int i = 0; i < 16; i++) {
        m_data[i] = data[i];
    }
}

float& Matrix::operator()(int row, int col)
{
    assert(row >= 0 && row < 4 && col >= 0 && col < 4);
    return m_data[col * 4 + row];
}

float Matrix::operator()(int row, int col) const
{
    assert(row >= 0 && row < 4 && col >= 0 && col < 4);
    return m_data[col * 4 + row];
}

const float* Matrix::toArray() const
{
    return m_data;
}

const Matrix Matrix::invert() const
{
    float det = determinant();
    assert(fabs(det) > EPSILON);

    Matrix result;

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            float sign = (((row + col) & 0x1) == 0x0) ? 1.0f : -1.0f;
            result.m_data[col * 4 + row] = sign * determinant(col, row) / det;
        }
    }

    return result;
}

const Matrix Matrix::transpose() const
{
    Matrix result(*this);

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            int src = col * 4 + row;
            int dest = row * 4 + col;

            float temp = result.m_data[src];
            result.m_data[src] = result.m_data[dest];
            result.m_data[dest] = temp;
        }
    }

    return result;
}

float Matrix::determinant() const
{
    float det = 0.0f;

    for (int col = 0; col < 4; col++) {
        float sign = ((col & 0x1) == 0x0) ? 1.0f : -1.0f;
        det += sign * operator()(0, col) * determinant(0, col);
    }

    return det;
}

float Matrix::determinant(int row, int col) const
{
    assert(row >= 0 && row < 4 && col >= 0 && col < 4);

    float data[9];
    int current = 0;

    for (int index = 0; index < 16; index++) {
        if ((index / 4) == col || (index % 4) == row) {
            continue;
        } else {
            data[current++] = m_data[index];
        }
    }

    /* 
        The newly created 3x3 matrix is also in column-major
        form:

        d0 d3 d6
        d1 d4 d7
        d2 d5 d8
    */

    return
        data[0] * (data[4] * data[8] - data[7] * data[5]) -
        data[1] * (data[3] * data[8] - data[6] * data[5]) +
        data[2] * (data[3] * data[7] - data[6] * data[4]);
}

const Matrix Matrix::multiplyRight(const Matrix& matrix) const
{
    Matrix result;

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            float curr = 0.0f;
            for (int i = 0; i < 4; i++) {
                curr += (operator()(row, i) * matrix(i, col));
            }
            result.m_data[col * 4 + row] = curr;
        }
    }

    return result;
}

const Matrix Matrix::multiplyLeft(const Matrix& matrix) const
{
    Matrix result;

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            float curr = 0.0f;
            for (int i = 0; i < 4; i++) {
                curr += (matrix(row, i) * operator()(i, col));
            }
            result.m_data[col * 4 + row] = curr;
        }
    }

    return result;
}

const Vector Matrix::multiplyRight(const Vector& vector) const
{
    return vector.multiplyLeft(*this);
}

ostream& operator<< (ostream& out, const Matrix& matrix)
{
    for (int row = 0; row < 4; row++) {
        out << "|";
        for (int col = 0; col < 4; col++) {
            out << matrix(row, col) << " ";
        }
        out << "|" << endl;
    }

    return out;
}
