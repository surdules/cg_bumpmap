
#ifndef __VECTOR_H
#define __VECTOR_H

#include <ostream>

class Matrix;

/**
    @file Vector.h
    @brief Declares the Vector class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/**
    @brief Defines a Vector consisting of X, Y, Z and W components.
*/
class Vector {
private:
    /** @brief The data array containing the X, Y, Z and W values. */
    float m_data[4];

private:
    /** @brief Initialize the components to the specified values. */
    void initialize(float x, float y, float z, float w);

public:
    /** @brief The X coordinate index in the array representing this Vector. */
    static const int X;
    /** @brief The Y coordinate index in the array representing this Vector. */
    static const int Y;
    /** @brief The Z coordinate index in the array representing this Vector. */
    static const int Z;
    /** @brief The W coordinate index in the array representing this Vector. */
    static const int W;

    /** @brief Constant vector representing the origin (0, 0, 0, 1). */
    static const Vector ORIGIN;

    /** 
        @brief Default constructor, initializes the X, Y, Z components to 0 
        and W to 1.
    */
    Vector();
    /** 
        @brief Initializes the X, Y, Z components to the specified values
        and W to 1.
    */
    Vector(float x, float y, float z);
    /** @brief Initializes all the components to the specified values. */
    Vector(float x, float y, float z, float w);

    /** @brief Mutable accessor for a Vector component. */
    float& operator[](int which);

    /** @brief Immutable accessor for a Vector component. */
    float operator[](int which) const;

    /** @brief Accessor for the raw Vector data. */
    const float* toArray() const;

    /**
        @brief Normalize the Vector: divide all components by the 
        Vector #length.
    */
    const Vector normalize() const;

    /** 
        @brief Returns the length of the Vector.

        The length is computed by taking the square root of the sum
        of the squares of the X, Y and Z Vector components.
    */
    float length() const;

    /**
        @brief Computes the dot product between this Vector and
        another Vector.
    */
    float dot(const Vector& vert) const;

    /**
        @brief Computes the cross product between this Vector and
        another Vector.
    */
    const Vector cross(const Vector& vert) const;

    /**
        @brief Computes the angle (in radians) between this Vector
        and another Vector.

        The angle is computed as follows:<br>
        <code>
        acos(this.dot(vert) / (this.length() * vert.length()))
        </code>
    */
    float angle(const Vector& vert) const;

    /**
        @brief Computes the sum between the X, Y and Z components of
        this Vector and another Vector's.
    */
    const Vector add(const Vector& vert) const;

    /**
        @brief Computes the difference between the X, Y and Z
        components of this Vector and another Vector's.
    */
    const Vector subtract(const Vector& vert) const;

    /** @brief Scales this vector's X, Y, and Z components by a given factor. */
    const Vector scale(float scale) const;

    /**
        @brief Transform this Vector by multiplying it on the left with the 
        given Matrix.

        The resulting Vector's X, Y, and Z components are scaled by 1/W
        and the W component is set to 1.
    */
    const Vector multiplyLeft(const Matrix& matrix) const;

    /**
        @brief Range compress the Vector to make it suitable for
        storage in a texture or a cubemap.

        The range compression is done by adding 1 to the X, Y, and Z
        Vector components, dividing them by 2 and finally multiplying
        them by the scale parameter. The reverse operation is performed 
        to uncompress the coordinates (for example, inside a pixel shader 
        program).

        This method assumes that the X, Y, and Z components have values
        ranging from -1.0 to 1.0; the compression allows negative values
        to be represented in (otherwise) unsigned texture components (in
        which case scale would be 255: the maximum range for a texture
        component value).
    */
    const Vector rangeCompress(float scale = 1.0f) const;

    /**
        @brief Insertion operator for this Vector into an output
        stream.
    */
    friend std::ostream& operator<< (std::ostream& out, const Vector& vect);
};

#endif // __VECTOR_H
