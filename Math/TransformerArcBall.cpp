
#include <assert.h>
#include <math.h>

#include "TransformerArcBall.h"

TransformerArcBall::TransformerArcBall(int width, int height) {
    reshape(width, height);
}

void TransformerArcBall::reshape(int width, int height) {
    assert((width > 0) && (height > 0));

    m_centerX = width / 2.0f;
    m_centerY = height / 2.0f;
    m_radius = (float) ceil(sqrt(m_centerX * m_centerX + m_centerY * m_centerY));
}

float TransformerArcBall::sphereZ(float x, float y) {
    float legX = x - m_centerX;
    float legY = y - m_centerY;
    return (float) sqrt(m_radius * m_radius - (legX * legX + legY * legY));
}

void TransformerArcBall::start(float x, float y) {
    m_rotationVector = Vector(x, y, sphereZ(x, y));
}

const Matrix TransformerArcBall::end(float x, float y) {
    float legX = x - m_centerX;
    float legY = y - m_centerY;
    float radius = (float) sqrt(legX * legX + legY * legY);
    if (radius > m_radius) {
        x = m_centerX + legX * (m_radius / radius);
        y = m_centerY + legY * (m_radius / radius);
    }

    Vector currentVector(x, y, sphereZ(x, y));

    float angle = 2.0f * m_rotationVector.angle(currentVector);

    Vector axis = m_rotationVector.cross(currentVector).normalize();

    return Quaternion(angle, axis).normalize().toMatrix();
}
