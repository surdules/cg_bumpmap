
#define WIN32_LEAN_AND_MEAN 
#include <windows.h>

#include <GL/gl.h>

#include "Application.h"

Application::Application():
    m_lightAmbientColor(0.1f, 0.1f, 0.1f),
    m_lightDiffuseColor(1.0f, 1.0f, 1.0f),
    m_lightPosition(0.0f, 12.0f, 12.0f),
    m_eyePosition(0.0f, 0.0f, 10.0f)
{
    // nop
}

void Application::init(const char* modelFileName)
{
    glLightfv(GL_LIGHT0, GL_AMBIENT, m_lightAmbientColor.toArray());
    glLightfv(GL_LIGHT0, GL_DIFFUSE, m_lightDiffuseColor.toArray());
    glLightfv(GL_LIGHT0, GL_POSITION, m_lightPosition.toArray());

    glEnable(GL_LIGHT0);

    m_model.loadModel(modelFileName);
}

const Vector& Application::getEyePosition() const
{
    return m_eyePosition;
}

Model& Application::getModel() 
{
    return m_model;
}

void Application::draw() const
{
    m_model.draw(m_eyePosition, m_lightPosition);
}

