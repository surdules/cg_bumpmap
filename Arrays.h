
#ifndef __ARRAYS_H
#define __ARRAYS_H

#include <assert.h>

/**
    @file Arrays.h
    @brief Wrappers allowing multi-dimensional access to one-dimensional arrays.

    Copyright (c) 2000 Cass Everitt<br>
    Copyright (c) 2000 NVIDIA Corporation<br>
    All rights reserved.

    Redistribution and use in source and binary forms, with or
    without modification, are permitted provided that the following
    conditions are met:

    <ul>
     <li>Redistributions of source code must retain the above
       copyright notice, this list of conditions and the following
       disclaimer.</li>

     <li>Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials
       provided with the distribution.</li>

     <li>The names of contributors to this software may not be used
       to endorse or promote products derived from this software
       without specific prior written permission.</li>
    </ul>

       THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
       ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
       LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
       FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
       REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
       INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
       BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
       LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
       CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
       LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
       ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
       POSSIBILITY OF SUCH DAMAGE.

    Cass Everitt - cass@r3.nu

    @author <A HREF="mailto:cass@r3.nu">Cass W. Everitt</A>
    @version 1.0
    @date 2/15/2003
*/

/** @brief Expose a one-dimensional array as a two-dimensional array. */
template <class T> class Array2 {
private:
    /** @brief The one-dimensional array of data */
    T* m_data;
    /** @brief Dimension of the one-dimensional array */
    int m_width, m_height;
    /** 
        @brief Was the array of data allocated inside this class? 

        If true, the data should be deleted when this class is
        deleted; otherwise, do nothing: the lifetime of the array
        of data is governed externally.
    */
    bool m_managed;

public:
    /** @brief Allocate a two-dimensional array. */
    Array2(int width, int height) {
        assert(width > 0 && height > 0);

        m_width = width;
        m_height = height;

        m_data = new T[m_width * m_height];

        m_managed = true;
    }

    /** @brief Bind to an existing two-dimensional array. */
    Array2(int width, int height, T* data) {
        assert(width > 0 && height > 0 && data != 0);

        m_width = width;
        m_height = height;

        m_data = data;

        m_managed = false;
    }

    /** @brief If previously allocated a two-dimensional array, free it. */
    ~Array2() {
        if (m_managed) {
            delete [] m_data;
        }
    }

    /** @brief Return m_data[row * m_width + col] */
    inline T& operator () (int col, int row) {
        assert(row >= 0 && row < m_height);
        assert(col >= 0 && col < m_width);
        return m_data[row * m_width + col];
    }

    /** @brief Return m_data[row * m_width + col] */
    inline const T& operator() (int col, int row) const {
        assert(row >= 0 && row < m_height);
        assert(col >= 0 && col < m_width);
        return m_data[row * m_width + col];
    }

    /** @brief Retrieve the array width. */
    int getWidth() const {
        return m_width;
    }

    /** @brief Retrieve the array height. */
    int getHeight() const {
        return m_height;
    }

    /** @brief Retrieve the array contents. */
    const T* getData() const {
        return m_data;
    }
};

/** @brief Expose a one-dimensional array as a three-dimensional array. */
template <class T> class Array3 {
private:
    /** @brief The one-dimensional array of data */
    T* m_data;
    /** @brief Dimension of the one-dimensional array */
    int m_width, m_height, m_depth;
    /** 
        @brief Was the array of data allocated inside this class? 

        If true, the data should be deleted when this class is
        deleted; otherwise, do nothing: the lifetime of the array
        of data is governed externally.
    */
    bool m_managed;

public:
    /** @brief Allocate a three-dimensional array. */
    Array3(int width, int height, int depth) {
        assert(width > 0 && height > 0 && depth > 0);

        m_width = width;
        m_height = height;
        m_depth = depth;

        m_data = new T[m_width * m_height * m_depth];

        m_managed = true;
    }

    /** @brief Bind to an existing three-dimensional array. */
    Array3(int width, int height, int depth, T* data) {
        assert(width > 0 && height > 0 && depth > 0 && data != 0);

        m_width = width;
        m_height = height;
        m_depth = depth;

        m_data = data;

        m_managed = false;
    }

    /** @brief If previously allocated a three-dimensional array, free it. */
    ~Array3() {
        if (m_managed) {
            delete [] m_data;
        }
    }

    /** @brief Return m_data[(row * m_width + col) * m_depth + depth] */
    inline T& operator () (int col, int row, int depth) {
        assert(row >= 0 && row < m_height);
        assert(col >= 0 && col < m_width);
        assert(depth >= 0 && depth < m_depth);
        return m_data[(row * m_width + col) * m_depth + depth];
    }

    /** @brief Return m_data[(row * m_width + col) * m_depth + depth] */
    inline const T& operator() (int col, int row, int depth) const {
        assert(row >= 0 && row < m_height);
        assert(col >= 0 && col < m_width);
        assert(depth >= 0 && depth < m_depth);
        return m_data[(row * m_width + col) * m_depth + depth];
    }

    /** @brief Retrieve the array width. */
    int getWidth() const {
        return m_width;
    }

    /** @brief Retrieve the array height. */
    int getHeight() const {
        return m_height;
    }

    /** @brief Retrieve the array depth. */
    int getDepth() const {
        return m_depth;
    }

    /** @brief Retrieve the array contents. */
    const T* getData() const {
        return m_data;
    }
};

#endif // __ARRAYS_H
