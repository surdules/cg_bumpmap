
#include <iostream>

#include <GL/glut.h>

#include "GLproc.h"

using namespace std;

PFNGLMULTITEXCOORD1FARBPROC glMultiTexCoord1fARB = NULL;
PFNGLMULTITEXCOORD2FARBPROC glMultiTexCoord2fARB = NULL;
PFNGLMULTITEXCOORD3FARBPROC glMultiTexCoord3fARB = NULL;
PFNGLMULTITEXCOORD4FARBPROC glMultiTexCoord4fARB = NULL;

PFNGLACTIVETEXTUREARBPROC glActiveTextureARB = NULL;
PFNGLCLIENTACTIVETEXTUREARBPROC glClientActiveTextureARB = NULL;

PFNGLSECONDARYCOLOR3FEXTPROC glSecondaryColor3fEXT = NULL;
PFNGLSECONDARYCOLOR3FVEXTPROC glSecondaryColor3fvEXT = NULL;

void initExtensions()
{
    if (!glutExtensionSupported("GL_ARB_multitexture")) {
        cerr << "initExtensions(): 'GL_ARB_multitexture' extension not supported" << endl;
        exit(1);
    }

    if (!glutExtensionSupported("GL_EXT_texture_env_combine")) {
        cerr << "initExtensions(): 'GL_EXT_texture_env_combine' extension not supported" << endl;
        exit(1);
    }

    if (!glutExtensionSupported("GL_EXT_secondary_color")) {
        cerr << "initExtensions(): 'GL_EXT_secondary_color' extension not supported" << endl;
        exit(1);
    }

    int maxTexelUnits;
    glGetIntegerv(GL_MAX_TEXTURE_UNITS_ARB, &maxTexelUnits);

    if (maxTexelUnits <= 1) {
        cerr << "initExtensions(): the hardware does not support more than 1 (multi-)texture unit" << endl;
        exit(1);
    }

    glMultiTexCoord1fARB = (PFNGLMULTITEXCOORD1FARBPROC) wglGetProcAddress("glMultiTexCoord1fARB");
    glMultiTexCoord2fARB = (PFNGLMULTITEXCOORD2FARBPROC) wglGetProcAddress("glMultiTexCoord2fARB");
    glMultiTexCoord3fARB = (PFNGLMULTITEXCOORD3FARBPROC) wglGetProcAddress("glMultiTexCoord3fARB");
    glMultiTexCoord4fARB = (PFNGLMULTITEXCOORD4FARBPROC) wglGetProcAddress("glMultiTexCoord4fARB");

    glActiveTextureARB = (PFNGLACTIVETEXTUREARBPROC) wglGetProcAddress("glActiveTextureARB");
    glClientActiveTextureARB = (PFNGLCLIENTACTIVETEXTUREARBPROC) wglGetProcAddress("glClientActiveTextureARB");

    glSecondaryColor3fEXT = (PFNGLSECONDARYCOLOR3FEXTPROC) wglGetProcAddress("glSecondaryColor3fEXT");
    glSecondaryColor3fvEXT = (PFNGLSECONDARYCOLOR3FVEXTPROC) wglGetProcAddress("glSecondaryColor3fvEXT");
}
