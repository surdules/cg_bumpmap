
#ifndef __APPLICATION_H
#define __APPLICATION_H

#include "Color.h"

#include "Vector.h"
#include "Matrix.h"

#include "Model.h"

/**
    @file Application.h
    @brief Declares the Application class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** @brief Defines the Application we are about to run. */
class Application {
private:
    /** @brief The color of the ambient light. */
    Color m_lightAmbientColor;
    /** @brief The color of the diffuse light. */
    Color m_lightDiffuseColor;
    /** @brief The position of the diffuse light. */
    Vector m_lightPosition;
    /** @brief The position of the eye. */
    Vector m_eyePosition;

    /** @brief The Model to draw on the screen. */
    Model m_model;
    
public:
    /** @brief Initialize application state. */
    Application();
    
    /** 
        @brief Setup OpenGL state and load the Model.
    */
    void init(const char* modelFileName);

    /** @brief Retrieve the eye position. */
    const Vector& getEyePosition() const;

    /** @brief Retrieve the Model drawn by this Application. */
    Model& getModel();

    /** @brief Draw the Model. */
    void draw() const;
};

#endif // __APP_H
